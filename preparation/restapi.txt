/players/{player_id}
GET /players/{player_id}
POST /players/{player_id}
PUT /players/{player_id}
DELETE /players/{player_id}
[
  {
    "id": "1",
    "nickname": "King",
    "gender": "male",
    "matches": "10",
    "wins": "7",
    "loses": "3",
    "rating": "8",
    "job": "fitness coach",
    "description": "",
    "status": "offline",
    "avatar": "./images/image.png"
  }
]

/matches/{match_id}
GET /matches/{match_id}
POST /matches/{match_id}
DELETE /matches/{match_id}
[
  {
    "id": "123",
    "players count": "2",
    "players count left": "2",
    "duration time": "5 mins",
    "players": [
	{
    	"id": "1",
    	"nickname": "King",
    	"gender": "male",
    	"matches": "10",
    	"wins": "7",
    	"loses": "3",
    	"rating": "8",
    	"job": "fitness coach",
    	"description": "",
    	"status": "offline",
    	"avatar": "./images/image.png"
  	},

	{
    	"id": "3",
    	"nickname": "dimon",
    	"gender": "male",
    	"matches": "2",
    	"wins": "2",
    	"loses": "0",
    	"rating": "6",
    	"job": "architect",
    	"description": "",
    	"status": "online",
    	"avatar": "./images/image.png"
  	}
    ]
  }
]

/matches/{match_id}/result
GET /matches/{match_id}/result
POST /players/{player_id}
[
  {
    "id": "2",
    "nickname": "qwerty2019",
    "result": "win",
    "matches": "8",
    "wins": "6",
    "loses": "2",
    "rating": "13",
    "job": "maths student",
    "status": "online",
    "avatar": "./images/image.png"
  }
]

/matches/{match_id}/{message_id}
GET /matches/{match_id}/{message_id}
POST /matches/{match_id}/{message_id}
PUT /matches/{match_id}/{message_id}
DELETE /matches/{match_id}/{message_id}
[
  {
    "id": "228",
    "nickname": "King",
    "gender": "male",
    "avatar": "./images/image.png",
    "time": "00:34",
    "text": "Hi"
  },

  {
    "id": "229",
    "nickname": "Lizard",
    "gender": "male",
    "avatar": "./images/image.png",
    "time": "00:35",
    "text": "Hey there"
  }
]

/rating
GET /rating/{player_id}
POST /rating/{player_id}
PUT /rating/{player_id}
[
  {
    "id": "1",
    "nickname": "King",
    "gender": "male",
    "matches": "10",
    "wins": "7",
    "loses": "3",
    "rating": "8",
    "job": "fitness coach",
    "description": "",
    "status": "offline",
    "avatar": "./images/image.png"
  }
]

/reviews
GET /reviews/{review_id}
POST /reviews/{review_id}
PUT /reviews/{review_id}
[
  {
    "id": "777",
    "nickname": "Anonymous",
    "avatar": "./images/image.png",
    "time": "00:34",
    "text": "Nice game"
  },

  {
    "id": "778",
    "nickname": "Anonymous",
    "avatar": "./images/image.png",
    "time": "00:35",
    "text": "Cool!"
  }
]